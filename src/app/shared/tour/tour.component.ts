import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tour',
  templateUrl: './tour.component.html',
  styleUrls: ['./tour.component.scss']
})
export class TourComponent implements OnInit {
  tourList = [{
    heading: 'Recommendation Tour 1',
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean luctus tristique felis et viverra.'
  }];
  constructor() { }

  ngOnInit() {
  }

}
