import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  footerRow1 = [{
    name: 'News'
  }, {
    name: 'Careers'
  }, {
    name: 'MSOS Search'
  }, {
    name: 'Transport Information Search'
  }, {
    name: 'A-Z Index Search'
  }, {
    name: 'Contact 3M'
  }];
  footerRow2 = [{
    name: 'Legal Information'
  }, {
    name: 'Privacy Policy'
  }];
  constructor() { }

  ngOnInit() {
  }

}
