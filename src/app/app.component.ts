import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angulardemo';
  blockList = [{
    heading: 'Tier 1 Story Headline',
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean luctus tristique felis et viverra.',
    image: true
  }, {
    heading: 'Tier 1 Story Headline',
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean luctus tristique felis et viverra.',
    image: true
  }, {
    heading: 'Tier 1 Story Headline',
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean luctus tristique felis et viverra.',
    image: true
  }, {
    heading: 'Tier 1 Story Headline',
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean luctus tristique felis et viverra.',
    image: true
  }, {
    heading: 'Tier 1 Story Headline',
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean luctus tristique felis et viverra.',
    image: true
  }, {
    heading: 'Tier 1 Story Headline',
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean luctus tristique felis et viverra.',
    image: true
  }];
  inlineList = [{
    heading: 'Tier 1 Story Headline',
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean luctus tristique felis et viverra.',
    image: true,
    fullStoryMargin: true
  }, {
    heading: 'Tier 1 Story Headline',
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean luctus tristique felis et viverra.',
    image: true,
    fullStoryMargin: true
  }, {
    heading: 'Tier 1 Story Headline',
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean luctus tristique felis et viverra.',
    image: true,
    fullStoryMargin: true
  }, {
    heading: 'Tier 1 Story Headline',
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean luctus tristique felis et viverra.',
    image: true,
    fullStoryMargin: true
  }, {
    heading: 'Tier 1 Story Headline',
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean luctus tristique felis et viverra.',
    image: true,
    fullStoryMargin: true
  }, {
    heading: 'Tier 1 Story Headline',
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean luctus tristique felis et viverra.',
    image: true,
    fullStoryMargin: true
  }, {
    heading: 'Tier 3 Story Headline',
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean luctus tristique felis et viverra.',
    image: false
  }];
}
